/**
 * @file   hist_fftable_nodal_field.hh
 *
 * @author David S. Kammer <dkammer@ethz.ch>
 * @author Gabriele Albertini <ga288@cornell.edu>
 * @author Chun-Yu Ke <ck659@cornell.edu>
 *
 * @date creation: Fri Feb 5 2021
 * @date last modification: Fri Feb 5 2021
 *
 * @brief  TODO
 *
 *
 * Copyright (C) 2021 ETH Zurich (David S. Kammer)
 *
 * This file is part of uguca.
 *
 * uguca is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * uguca is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with uguca.  If not, see <https://www.gnu.org/licenses/>.
 */
/* -------------------------------------------------------------------------- */
#ifndef __HIST_FFTABLE_NODAL_FIELD_H__
#define __HIST_FFTABLE_NODAL_FIELD_H__
/* -------------------------------------------------------------------------- */
#include "fftable_nodal_field.hh"

#include "modal_limited_history.hh"
//#include "convolutions.hh"

#include <memory>

/* -------------------------------------------------------------------------- */

__BEGIN_UGUCA__
class HistFFTableNodalField : public FFTableNodalField {

  friend class BaseIO;

  /* ------------------------------------------------------------------------ */
  /* Typedefs                                                                 */
  /* ------------------------------------------------------------------------ */
protected:
  typedef std::vector<ModalLimitedHistory> LHVector;

  /* ------------------------------------------------------------------------ */
  /* Constructors/Destructors                                                 */
  /* ------------------------------------------------------------------------ */
public:
  HistFFTableNodalField(const std::string & name = "unnamed")
    : FFTableNodalField(name) {}

  HistFFTableNodalField(FFTableMesh & mesh,
			SpatialDirectionSet components = {0},
			const std::string & name = "unnamed");

  virtual ~HistFFTableNodalField() {}

private:
  // private copy constructor: NodalField cannot be copied (for now to debug)
  HistFFTableNodalField(HistFFTableNodalField & to_copy);

  /* ------------------------------------------------------------------------ */
  /* Methods                                                                  */
  /* ------------------------------------------------------------------------ */
public:
  // adds current value to history (for all modes and dimensions)
  void addCurrentValueToHistory(unsigned int add_count = 1);

  // add current value of a different vector to history
  // (needed when predicting)
  void addCurrentValueToHistory(FFTableNodalField & other,
				unsigned int add_count = 1);

  // change current value to history (for all modes and dimensions)
  void changeCurrentValueOfHistory(unsigned int change_count = 1);

  // fills history with current value (for all modes and dimensions)
  void fillHistoryWithCurrentValue();

  // fills history with current value of different vector
  // (needed when predicting)
  void fillHistoryWithCurrentValue(FFTableNodalField & other);
  
  // extend the history to at least this size
  void extendHistory(unsigned int size);
  
  /* ------------------------------------------------------------------------ */
  /* Accessors                                                                */
  /* ------------------------------------------------------------------------ */
public:
  // get ModalLimitedHistory of frequency domain in direction d
  inline const ModalLimitedHistory & hist(int f, int d=0) const;
  inline ModalLimitedHistory & hist(int f, int d=0);
  
  /* ------------------------------------------------------------------------ */
  /* Class Members                                                            */
  /* ------------------------------------------------------------------------ */
protected:
  // start indices for each component
  std::vector<int> hist_start;
  
  // past values of field in frequency domain
  // each LimitedHistory is for a given dimension d and wave number q
  LHVector hist_storage;
};

/* -------------------------------------------------------------------------- */
/* inline functions                                                           */
/* -------------------------------------------------------------------------- */
inline const ModalLimitedHistory & HistFFTableNodalField::hist(int f, int d) const {
  return this->hist(f,d);
}

inline ModalLimitedHistory & HistFFTableNodalField::hist(int f, int d) {
  if (!this->components.count(d)) 
    throw std::runtime_error("HistFFTableNodalField "
			     +this->name
			     +" has no component "
			     +std::to_string(d)+"\n");
  // needs to be structured as fd_storage
  return this->hist_storage[this->hist_start[d]+f]; 
}

__END_UGUCA__

#endif /* __HIST_FFTABLE_NODAL_FIELD_H__ */
