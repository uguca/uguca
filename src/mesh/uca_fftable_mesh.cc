/**
 * @file   uca_fftable_mesh.cc
 *
 * @author David S. Kammer <dkammer@ethz.ch>
 * @author Gabriele Albertini <ga288@cornell.edu>
 * @author Chun-Yu Ke <ck659@cornell.edu>
 *
 * @date creation: Fri Feb 5 2021
 * @date last modification: Fri Feb 5 2021
 *
 * @brief  TODO
 *
 *
 * Copyright (C) 2021 ETH Zurich (David S. Kammer)
 *
 * This file is part of uguca.
 *
 * uguca is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * uguca is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with uguca.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "uca_fftable_mesh.hh"
#include "fftable_nodal_field.hh"
#include "static_communicator_mpi.hh"

#include <cmath>
#include <stdexcept>

__BEGIN_UGUCA__

/* -------------------------------------------------------------------------- */
FFTableMesh::FFTableMesh(double Lx, int Nx) :
  BaseMesh(2, Nx),
  lengths({Lx,0,0}),
  nb_nodes_global({Nx,1,1}),
  wave_numbers_local(3,0),
  mode_zero_rank(0),
  mode_zero_index(0) {

  this->nb_fft_global = {this->nb_nodes_global[0] / 2 + 1,1,1};

  this->resize(this->getNbGlobalFFT());
  this->initWaveNumbersGlobal(this->wave_numbers_local);
}

/* -------------------------------------------------------------------------- */
FFTableMesh::FFTableMesh(double Lx, int Nx,
			 double Lz, int Nz) :
  BaseMesh(3, Nx*Nz),
  lengths({Lx,0,Lz}),
  nb_nodes_global({Nx,1,Nz}),
  wave_numbers_local(3,0),
  mode_zero_rank(0),
  mode_zero_index(0) {

  this->nb_fft_global = {this->nb_nodes_global[0], 1, this->nb_nodes_global[2] / 2 + 1};

  this->resize(this->getNbGlobalFFT());
  this->initWaveNumbersGlobal(this->wave_numbers_local);
}

/* -------------------------------------------------------------------------- */
FFTableMesh::~FFTableMesh() {

  int prank = StaticCommunicatorMPI::getInstance()->whoAmI();
  if (prank == this->root) {
    
    for (unsigned int i=0; i<this->forward_plans.size(); ++i) {
      fftw_destroy_plan(this->forward_plans[i]);
      fftw_destroy_plan(this->backward_plans[i]);
      // do not call fftw_cleanup() because we don't know if all plans are destroyed
    }
  }
}

/* -------------------------------------------------------------------------- */
void FFTableMesh::resize(int nb_fft, int alloc) {
  this->nb_fft_local = nb_fft;
  this->nb_fft_local_alloc = (alloc < nb_fft ? nb_fft : alloc);
  this->wave_numbers_local.resize(this->nb_fft_local_alloc);
}

/* -------------------------------------------------------------------------- */
void FFTableMesh::registerForFFT(FFTableNodalField & nodal_field) {

  int prank = StaticCommunicatorMPI::getInstance()->whoAmI();
  if (prank == this->root) {

    // loop over all components of the nodal field
    for (const auto& d : nodal_field.getComponents()) {

      // access relevant storage
      double * nfc = nodal_field.data(d);
      fftw_complex * fd_nfc = nodal_field.fd_data(d);

      // set plans to NULL
      fftw_plan forward_plan = NULL;
      fftw_plan backward_plan = NULL;

      // create fftw plans for relevant dimension
      if (this->dim==2) {
	forward_plan = fftw_plan_dft_r2c_1d(this->nb_nodes_global[0],
					    nfc, fd_nfc, FFTW_MEASURE);
	backward_plan = fftw_plan_dft_c2r_1d(this->nb_nodes_global[0],
					     fd_nfc, nfc, FFTW_MEASURE);
      }
      else if (this->dim==3) {
	forward_plan = fftw_plan_dft_r2c_2d(this->nb_nodes_global[0],
					    this->nb_nodes_global[2],
					    nfc, fd_nfc, FFTW_MEASURE);
	backward_plan = fftw_plan_dft_c2r_2d(this->nb_nodes_global[0],
					     this->nb_nodes_global[2],
					     fd_nfc, nfc, FFTW_MEASURE);
      }
      this->forward_plans.push_back(forward_plan);
      this->backward_plans.push_back(backward_plan);
    
      // set the fftw_plan_id of the nodal field component (can do because friend)
      nodal_field.fftw_plan_ids[d] = this->forward_plans.size()-1;
    }
  }
}

/* -------------------------------------------------------------------------- */
void FFTableMesh::unregisterForFFT(FFTableNodalField & nodal_field) {
  
  int prank = StaticCommunicatorMPI::getInstance()->whoAmI();
  if (prank == this->root) {
    // loop over all components of the nodal field
    for (const auto& d : nodal_field.getComponents()) {
      fftw_destroy_plan(this->forward_plans[nodal_field.getFFTWPlanId(d)]);
      fftw_destroy_plan(this->backward_plans[nodal_field.getFFTWPlanId(d)]);
    }
  }
}

/* -------------------------------------------------------------------------- */
void FFTableMesh::forwardFFT(FFTableNodalField & nodal_field) {
  int prank = StaticCommunicatorMPI::getInstance()->whoAmI();
  if (prank == this->root) {
    // loop over all components of the nodal field
    for (const auto& d : nodal_field.getComponents()) {
      fftw_execute(this->forward_plans[nodal_field.getFFTWPlanId(d)]);
    }
  }
}
  
/* -------------------------------------------------------------------------- */
void FFTableMesh::backwardFFT(FFTableNodalField & nodal_field) {
  
  int prank = StaticCommunicatorMPI::getInstance()->whoAmI();
  if (prank == this->root) {
    // loop over all components of the nodal field
    for (const auto& d : nodal_field.getComponents()) {

      fftw_execute(this->backward_plans[nodal_field.getFFTWPlanId(d)]);
    
      double nb_nodes_global = this->getNbGlobalNodes();
      double * p_field = nodal_field.data(d);
      for (int i=0; i<this->getNbLocalNodesAlloc(); ++i) { // for fftw_mpi it includes padding
	p_field[i] /= nb_nodes_global;
      }
    }
  }
}

/* -------------------------------------------------------------------------- */
/*
 * Serial version includes mode 0 (not used in computeStressFourierCoeff)
 * to avoid sorting before adding the zero mode back in
 * computeStressFourierCoeff()
 *
 */
void FFTableMesh::initWaveNumbersGlobal(TwoDVector & wave_numbers) {

  // fundamental mode
  double k1 = 2*M_PI / this->lengths[0];
  double m1 = 0.0;
  if (this->dim == 3)
    m1 = 2*M_PI / this->lengths[2];

  // compute nyquest frequency
  int f_ny_x = this->nb_fft_global[0]; // 2D
  if (this->dim == 3)
    f_ny_x = this->nb_fft_global[0]/2+1;

  // init wave numbers global
  for (int i=0; i<this->nb_fft_global[0]; ++i) {
    for (int j=0; j<this->nb_fft_global[2]; ++j) {
      int ij =  i*this->nb_fft_global[2] + j;
      
      if (this-> dim == 2) {
	wave_numbers(ij,0) = k1 * i;
      }
      else {
	// after nyquest frequncy modes are negative
	// e.g. 0, 1, 2, ... ny, -ny, -ny+1, ... -2, -1
	wave_numbers(ij,0) = k1 * (i - (i/f_ny_x)*this->nb_fft_global[0]);
	wave_numbers(ij,2) = m1 * j;
      }

      wave_numbers(ij,1) = 0.0; // we re on the xz plane
    }
  }
}

__END_UGUCA__
