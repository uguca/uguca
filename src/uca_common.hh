/**
 * @file   uca_common.hh
 *
 * @author David S. Kammer <dkammer@ethz.ch>
 * @author Gabriele Albertini <ga288@cornell.edu>
 * @author Chun-Yu Ke <ck659@cornell.edu>
 *
 * @date creation: Fri Feb 5 2021
 * @date last modification: Fri Feb 5 2021
 *
 * @brief  TODO
 *
 *
 * Copyright (C) 2021 ETH Zurich (David S. Kammer)
 *
 * This file is part of uguca.
 *
 * uguca is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * uguca is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with uguca.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __UCA_COMMON_H__
#define __UCA_COMMON_H__
#include <set>
#include <vector>
#include <complex>
#include "uca_config.hh"

#define __BEGIN_UGUCA__ namespace uguca {
#define __END_UGUCA__  }

namespace uguca {

  enum class Krnl { H00, H01, H11, H22 };

  enum SolverMethod {
    _static = 0,
    _quasi_dynamic = 1,
    _dynamic = 2,
    _adaptive = 3
  };

  enum SpatialDirection { _x = 0, _y = 1, _z = 2, _spatial_dir_count = 3 };
  using SpatialDirectionSet = std::set<int>;

  typedef std::vector<std::complex<double>> VecComplex;

} // namespace

#endif /* __UCA_COMMON_H__ */
